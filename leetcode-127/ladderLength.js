function ladderLength(beginWord, endWord, wordList) {
    const graph = {};
    const wordSet = new Set(wordList);
    graph[beginWord] = getNeighbouringWords(beginWord, wordSet);
    for (const word of wordList) {
        graph[word] = getNeighbouringWords(word, wordSet);
    }
    let queue = [{word: beginWord, pathLength: 0}];
    const visited = new Set();
    while (queue.length) {
        let {word: currentWord, pathLength} = queue.shift();
        if (visited.has(currentWord)) continue;
        if (currentWord === endWord) return pathLength + 1;
        visited.add(currentWord);
        graph[currentWord].map(w => queue.push({word: w, pathLength: pathLength+1}));
    }
    return 0;
}

function getNeighbouringWords(word, wordSet) {
    const neighbours = [];
    for(let i = 1; i < word.length+1; i++) {
        for(const char of 'abcdefghijklmnopqrstuvwxyz') {
            const testWord = word.slice(0,i-1) + char + word.slice(i);
            if (testWord != word && wordSet.has(testWord)) {
                neighbours.push(testWord);
            }
        }
    }
    return neighbours;
}

console.log(ladderLength('hit', 'cog', ['hot', 'dot', 'dog', 'lot', 'log', 'cog']));
// console.log(ladderLength("hot", "dog", ["hot","cog","dog","tot","hog","hop","pot","dot"]));