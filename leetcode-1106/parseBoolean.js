
function parseBoolExpr(expression) {
    const regexp = /(?<operator>[!&|])\((?<paren_phrase>[ft,]+)\)/;
    let match;
    while ((match = expression.match(regexp))) {
        const operator = match.groups.operator;
        const parentheticPhrase = match.groups.paren_phrase;
        const toReplace = expression.slice(match.index, match.index + operator.length + parentheticPhrase.length + 2);
        const parsedPhrase = parseSimpleExpression(operator, parentheticPhrase);
        expression = expression.slice(0, match.index) + parsedPhrase + expression.slice(match.index + toReplace.length);
    }
    return expression === 't' ? true : false;
}
function parseSimpleExpression(operator, expression){
    const booleans = expression.split(',').map(boolean => boolean === 't' ? true : false);
    let returnVal;
    if (operator === '!') {
        returnVal = !booleans[0];
    } else if (operator === '|') {
        returnVal = booleans.some(el => el);
    } else {
        returnVal = booleans.every(el => el);
    }
    return returnVal === true ? 't' : 'f';
}

